package de.agoerg.javarestapi.controller;

import de.agoerg.javarestapi.entity.Item;
import de.agoerg.javarestapi.entity.Items;
import de.agoerg.javarestapi.entity.base.ItemBase;
import de.agoerg.javarestapi.entity.base.ItemBaseRequestBody;
import de.agoerg.javarestapi.lib.MyJsonHandler;
import io.swagger.models.auth.In;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller für create, read, update und delete von Items.
 */
@RestController
public class ItemController {

    // region read all items

    /**
     * Gibt alle Items zurück.<br>
     * Methode: GET<br>
     * Restpunkt: /items
     *
     * @return Gibt eine Instanz der Klasse {@link Items} zurück.
     */
    @RequestMapping(
            method = RequestMethod.GET,
            path = "/items",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Items getItems() {
        // Default Error Rückgabewert
        Items items = new Items();
        // alle items aus der json-Datei lesen
        MyJsonHandler myJsonHandler = MyJsonHandler.getInstance();
        List<ItemBase> itemArray = myJsonHandler.readJson();
        if (itemArray != null) {
            // gewünschter Rückgabewert
            items = new Items(200, "Gelesen.", itemArray);
        }
        return items;
    }
    // endregion

    // region read item by id
    /**
     * Gibt ein Item zurück anhand der ID.<br>
     * Methode: GET<br>
     * Restpunkt: /items
     *
     * @return Gibt eine Instanz der Klasse {@link Item} zurück.
     */
    @RequestMapping(
            method = RequestMethod.GET,
            path = "/item",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Item getItem(@RequestParam(name = "id") String id) {
        // RequestParam
        Integer l_id = null;
        // Default Error Rückgabewert
        Item item = new Item();
        try {
            // RequestParam
            l_id = Integer.parseInt(id);
            // alle items aus der json-Datei lesen
            MyJsonHandler myJsonHandler = MyJsonHandler.getInstance();
            List<ItemBase> itemsArray = myJsonHandler.readJson();
            // flag
            ItemBase itemToReturn = null;
            for (ItemBase itemBase : itemsArray) {
                // RequestParam
                if (itemBase.getId() == l_id) {
                    // RequestBody
                    itemToReturn = itemBase;
                }
            }
            if (itemToReturn == null) {
                // Error Rückgabewert
                return errorGetItemWithIdNotThere(l_id);
            }
            // gewünschter Rückgabewert
            item = new Item(200, "Gelesen.", itemToReturn);
        } catch (NumberFormatException exception) {
            // exception.printStackTrace();
            exception.getMessage();
            // Error Rückgabewert
            return errorGetItemWithIdNotThere(id);
        }
        // Rückgabewert
        return item;
    }
    // endregion

    // region update

    /**
     * Ändert den Namen eines Items.<br>
     * Methode: PUT<br>
     * Restpunkt: /item<br>
     * Request-body-params (raw, json): int id, String name
     *
     * @param itemBaseRequestBody id und name werden als Requestparameter angenommen (siehe {@link ItemBase}).
     * @return Gibt eine geänderte Instanz der Klasse {@link Item} zurück.
     */
    @RequestMapping(
            method = RequestMethod.PUT,
            path = "/item",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Item updateItem(@RequestParam(name = "id") String id, @RequestBody ItemBaseRequestBody itemBaseRequestBody) {
        // RequestParam
        Integer l_id = null;
        // Default Error Rückgabewert
        Item itemReturn = new Item();
        ItemBase itemBase = new ItemBase();
        try {
            // RequestParam
            l_id = Integer.parseInt(id);
            // alle items aus der Json-Datei lesen
            MyJsonHandler myJsonHandler = MyJsonHandler.getInstance();
            List<ItemBase> items = myJsonHandler.readJson();
            // flag
            boolean bIdIsThere = false;
            for (ItemBase item : items) {
                // RequestParam
                if (item.getId() == l_id) {
                    // RequestBody
                    item.setName(itemBaseRequestBody.getName());
                    // flag
                    bIdIsThere = true;
                }
            }
            // flag
            if (bIdIsThere == false) {
                // Error Rückgabewert
                return this.errorGetItemWithIdNotThere(id);
            }
            // Änderungen speichen
            items = myJsonHandler.writeJson(items);
            for (ItemBase item : items) {
                // RequestParam
                if (item.getId() == l_id) {
                    itemBase.setId(item.getId());
                    itemBase.setName(item.getName());
                    // gewünschter Rückgabewert
                    itemReturn = new Item(200, "Geändert.", itemBase);
                }
            }
        }
        catch(NumberFormatException exception) {
            // exception.printStackTrace();
            exception.getMessage();
            // Error Rückgabewert
            return this.errorGetItemWithIdNotThere(id);
        }
        return itemReturn;
    }
    // endregion

    // region create

    /**
     * Legt ein neues Item an.<br>
     * Die Logik der Applikation schaut nach der höchsten ID<br>
     * und zählt diese einmal hoch.<br>
     * Methode: POST<br>
     * Restpunkt: /item<br>
     * Request-body-params (raw, json): String name
     *
     * @param itemBaseRequestBody id und name werden als RequestBody angenommen (siehe {@link ItemBase}).
     * @return Gibt das neu angelegte Item als Instanz der Klasse {@link Item} zurück.
     */
    @RequestMapping(
            method = RequestMethod.POST,
            path = "/item",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Item createItem(@RequestBody ItemBaseRequestBody itemBaseRequestBody) {
        // alle items aus der json-Datei lesen
        MyJsonHandler myJsonHandler = MyJsonHandler.getInstance();
        List<ItemBase> items = myJsonHandler.readJson();
        // Default Error Rückgabewert
        Item itemReturn = new Item();
        ItemBase itemBase = new ItemBase();
        // ID Auto Increment
        Integer id = this.getNewID(items);
        // Request mit Body enthält nur den Namen.
        // Daher die neue ID setzen.
        itemBase.setId(id);
        itemBase.setName(itemBaseRequestBody.getName());
        // Neues Item der Item-Liste hinzufügen.
        // Auskommentieren, um Nicht-Speichern zu simulieren.
        items.add(itemBase);
        // Die erweiterte Item-Liste in die JSON-Datei schreiben.
        items = myJsonHandler.writeJson(items);
        boolean bIdIsThere = false;
        for (ItemBase item : items) {
            if (item.getId() == itemBase.getId()) {
                // gewünschter Rückgabewert
                itemReturn = new Item(200, "Angelegt.", itemBase);
                bIdIsThere = true;
            }
        }
        if (bIdIsThere == false) {
            // Error Rückgabewert
            return this.getItemErrorWhileCreating(itemBase.getId());
        }
        return itemReturn;
    }
    // endregion

    // region delete

    /**
     * Löscht ein Item anhand der ID.<br>
     * Methode: DELETE<br>
     * Restpunkt: /item(Fragezeichen)id=10
     *
     * @param id Erhält die ID als Requestparameter.
     * @return Gibt eine Instanz der Klasse {@link Item}, welche aber nur die gelöschte ID enthält.
     */
    @RequestMapping(
            method = RequestMethod.DELETE,
            path = "/item",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Item deleteItem(@RequestParam(name = "id") String id) {
        // RequestParam
        Integer l_id = null;
        // Default Error Rückgabewert
        Item itemReturn = new Item();
        try {
            l_id = Integer.parseInt(id);
            // alle items aus der json-Datei lesen
            MyJsonHandler myJsonHandler = MyJsonHandler.getInstance();
            List<ItemBase> items = myJsonHandler.readJson();
            // item welches gelöscht werden soll
            ItemBase itemToBeDeleted = null;
            // flag
            boolean bIdIsThere = false;
            for (ItemBase item : items) {
                // RequestParam
                if (item.getId() == l_id) {
                    // item welches gelöscht werden soll
                    itemToBeDeleted = item;
                    bIdIsThere = true;
                }
            }
            // Error Rückgabewert
            if (bIdIsThere == false) {
                return this.errorGetItemWithIdNotThere(id);
            }
            // item löschen
            items.remove(itemToBeDeleted);
            // items in json-Datei schreiben
            items = myJsonHandler.writeJson(items);
            for (ItemBase item : items) {
                // Request Param
                if (item.getId() == l_id) {
                    // Error Rückgabewert
                    return new Item(201, "Error. Das Item wurde nicht gelöscht.", item);
                }
            }
            // gewünschter Rückgabewert
            itemReturn = new Item(200, "Gelöscht.", new ItemBase(l_id, null));
        } catch(NumberFormatException exception) {
                // exception.printStackTrace();
                exception.getMessage();
                // Error Rückgabewert
                return this.errorGetItemWithIdNotThere(id);
            }
        return itemReturn;
    }
    // endregion

    // region methods
    private Integer getNewID(List<ItemBase> items) {
        Integer id = 0;
        for (ItemBase item : items) {
            if (item.getId() > id) {
                id = item.getId();
            }
        }
        id += 1;
        return id;
    }

    private Item errorGetItemWithIdNotThere(Integer id) {
        return new Item(201, "Das Item mit der ID '" + id + "' wurde nicht gefunden.", null);
    }

    private Item errorGetItemWithIdNotThere(String id) {
        return new Item(201, "Das Item mit der ID '" + id + "' wurde nicht gefunden.", null);
    }

    private Item getItemErrorWhileCreating(Integer id) {
        return new Item(201, "Error beim Speichern. Das Item konnte nicht gespeichert werden.", null);
    }

    private Item getParseIntError() {
        return new Item(201, "Keine Ganzzahl.", null);
    }
    // endregion
}
