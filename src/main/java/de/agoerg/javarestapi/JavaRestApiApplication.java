package de.agoerg.javarestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Einstiegsklasse der Applikation. Enthält die Annotation @{@link SpringBootApplication}.
 */
@SpringBootApplication
@EnableSwagger2
public class JavaRestApiApplication {
	/**
	 * Statische Einstiegsmethode main der Applikation. Enthält die Klasse {@link SpringBootApplication} als Dependency-Injection.
	 * @param args Mögliche Eingabeaufforderungsparameter.
	 */
	public static void main(String[] args) {
		SpringApplication.run(JavaRestApiApplication.class, args);
	}

	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.paths(PathSelectors.any())
				.apis(RequestHandlerSelectors.basePackage( "de.agoerg.javarestapi" ))
				.build();
	}
}
