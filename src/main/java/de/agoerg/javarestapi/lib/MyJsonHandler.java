package de.agoerg.javarestapi.lib;

import com.google.gson.Gson;
import de.agoerg.javarestapi.entity.base.ItemBase;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Diese Klasse händelt das Lesen aus und das Schreiben in eine JSON-Datei.
 */
public class MyJsonHandler {

    private static final String FILE_NAME_IDE_READ  = "/data/items.json";
    private static final String FILE_NAME_IDE_WRITE = "data/items.json";
    private static final String FILE_NAME_JAR = "./data/items.json";
    private String protocol = null;
    private static MyJsonHandler instance = null;

    /**
     * Der Default-Konstruktor.
     */
    private MyJsonHandler() {
        this.protocol = this.getClass().getResource("").getProtocol();
    }

    /**
     * Ist ein Singleton-Pattern und gibt eine Instanz dieser Klasse zurück.
     * @return Das ist die Instanz dieser Klasse.
     */
    public static MyJsonHandler getInstance() {
        if(instance == null) {
            instance = new MyJsonHandler();
        }
        return instance;
    }

    /**
     * Liest alle Items aus der JSON-Datei.
     * @return Gibt alle Items als Liste der Klasse {@link ItemBase} zurück.
     */
    public List<ItemBase> readJson(){
        List<ItemBase> items = null;
        String json = "";
        try {
            BufferedReader reader = null;
            FileInputStream file = null;
            if(this.protocol == "file") {
                InputStream inputStream = getClass().getResourceAsStream(FILE_NAME_IDE_READ);
                reader = new BufferedReader(new InputStreamReader(inputStream));
                json = reader.lines().collect(Collectors.joining(System.lineSeparator()));
            } else if (this.protocol == "jar") {
                file = new FileInputStream(FILE_NAME_JAR);
                int index;
                while((index=file.read())!=-1){
                    json += (char)index;
                }
            }
            //
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(json);
            JSONArray itemList = (JSONArray) obj;

            if(reader != null) {
                reader.close();
            } else if (file != null) {
                file.close();
            }

            Integer id;
            String name;
            items = new ArrayList<>();
            for(Object object: itemList){
                // ToDo - Casten geht nicht
                // Done
                /*System.out.println("################################################");
                System.out.println(object);
                System.out.println(object.getClass());
                System.out.println((JSONObject)object);
                System.out.println(((JSONObject)object).getClass());
                System.out.println(((JSONObject)object).get("id"));
                System.out.println(((JSONObject)object).get("id").getClass());
                System.out.println("################################################");*/
                Long test_long_id = 0L;
                test_long_id = (Long)((JSONObject)object).get("id");
                id = test_long_id.intValue();
                name = (String)((JSONObject)object).get("name");
                items.add(new ItemBase(id, name));
            }
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
        } catch (IOException e) {
            System.out.println("IOException");
        } catch (ParseException e) {
            System.out.println("ParseException");
        }
        return items;
    }

    /**
     * Schreibt alle Items in die JSON-Datei.
     * @param items Nimmt die geänderten Item als Liste der Klasse {@link ItemBase } entgegen.
     * @return Gibt alle Items als Liste der Klasse {@link ItemBase} zurück.
     */
    public synchronized List<ItemBase> writeJson(List<ItemBase> items){
        // Items in JSON String umändern.
        Gson gson = new Gson();
        String json = gson.toJson(items);
        // Items in die JSON-Datei speichern.
        try {
            if(this.protocol == "file") {
                ClassLoader classLoader = getClass().getClassLoader();
                File file = new File(classLoader.getResource(FILE_NAME_IDE_WRITE).getFile());
                if(!file.exists()) {
                    throw new FileNotFoundException();
                }
                FileWriter writer = new FileWriter(file);
                writer.write(json);
                writer.flush();
                writer.close();
            } else if (this.protocol == "jar") {
                FileOutputStream file = new FileOutputStream(FILE_NAME_JAR);
                OutputStreamWriter writer = new OutputStreamWriter(file);
                writer.write(json);
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("IOException");
        }
        return this.readJson();
    }
}
