package de.agoerg.javarestapi.entity;

import de.agoerg.javarestapi.entity.base.ItemBase;

import java.util.ArrayList;
import java.util.List;

/**
 * Diese Klasse wird von der Restapi als Json-Objekt zurückgegeben.
 */
public class Items {
    private int code = 400;
    private String message = "Error. Oder noch keine Daten angelegt.";
    private List<ItemBase> items = new ArrayList<ItemBase>();

    /**
     * Default-Konstruktor, u.a. als Default Rest-Api-Rückgabewert.
     */
    public Items() {
    }

    /**
     * Konstruktor, welcher alle Attribute initialisiert.
     * @param code Der Status-Code. 200 bei erfolgreicher Rückgabe.
     * @param message Message des Status-Codes.
     * @param items Liste mit Instanzen der Klasse {@link ItemBase}.
     */
    public Items(int code, String message, List<ItemBase> items) {
        this.code = code;
        this.message = message;
        this.items = items;
    }

    /**
     * Gibt den Status-Code zurück.
     * @return Der Status-Code der Response.
     */
    public int getCode() {
        return code;
    }

    /**
     * Gibt die Message zurück.
     * @return Die Message der Response.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Enthält die eigentlichen Daten der Response.
     * @return Ist eine Liste mit Instanzen der Klasse {@link ItemBase}.
     */
    public List<ItemBase> getItems() {
        return items;
    }
}
