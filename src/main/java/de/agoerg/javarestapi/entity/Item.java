package de.agoerg.javarestapi.entity;

import de.agoerg.javarestapi.entity.base.ItemBase;

/**
 * Diese Klasse wird von der Restapi als Json-Objekt zurückgegeben.
 */
public class Item {
    private int code = 400;
    private String message = "Error.";
    private ItemBase item = null;

    /**
     * Default-Konstruktor, u.a. als Default Rest-Api-Rückgabewert.
     */
    public Item() {
    }

    /**
     * Konstruktor, welcher alle Attribute initialisiert.
     * @param code Der Status-Code. 200 bei erfolgreicher Rückgabe.
     * @param message Message des Status-Codes.
     * @param item Instanz der Klasse {@link ItemBase}.
     */
    public Item(int code, String message, ItemBase item) {
        this.code = code;
        this.message = message;
        this.item = item;
    }

    /**
     * Gibt den Status-Code zurück.
     * @return Der Status-Code der Response.
     */
    public int getCode() {
        return code;
    }

    /**
     * Gibt die Message zurück.
     * @return Die Message der Response.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Enthält die eigentlichen Daten der Response.
     * @return Ist eine Instanz der Klasse {@link ItemBase}.
     */
    public ItemBase getItem() {
        return item;
    }
}
