package de.agoerg.javarestapi.entity.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Basis-Entity-Klasse: Item
 */
public class ItemBaseRequestBody {
    private String name = null;

    /**
     * Default-Konstruktor, u.a. als Default Rest-Api-Rückgabewert
     */
    public ItemBaseRequestBody() {
    }

    /**
     * Konstruktor, welcher alle Attribute initialisiert
     * @param name Name des Items
     */
    public ItemBaseRequestBody(String name) {
        this.name = name;
    }

    /**
     * gibt den Namen des Items zurück
     * @return Name des Items
     */
    public String getName() {
        return name;
    }

    /**
     * zum Ändern des Namens
     * @param name Name der Entität
     */
    public void setName(String name) {
        this.name = name;
    }
}
