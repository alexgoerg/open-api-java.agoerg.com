package de.agoerg.javarestapi.entity.base;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Basis-Entity-Klasse: Item
 */
public class ItemBase {
    private Integer id = 0;
    private String name = null;

    /**
     * Default-Konstruktor, u.a. als Default Rest-Api-Rückgabewert
     */
    public ItemBase() {
    }

    /**
     * Konstruktor, welcher alle Attribute initialisiert
     * @param id ID des Items
     * @param name Name des Items
     */
    public ItemBase(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * gibt die ID des Items zurück
     * @return ID des Items
     */
    public Integer getId() {
        return id;
    }

    /**
     * gibt den Namen des Items zurück
     * @return Name des Items
     */
    public String getName() {
        return name;
    }

    /**
     * zum Ändern der ID
     * @param id ID des Items
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * zum Ändern des Namens
     * @param name Name der Entität
     */
    public void setName(String name) {
        this.name = name;
    }
}
